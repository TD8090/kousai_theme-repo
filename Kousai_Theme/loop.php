<?php
/**
 * @package WordPress
 * @subpackage kousai
 */
 /* Start the Loop */ ?>

<div class="loopclass" id="archive-content">
	<div id="inner wrapper" class="inner-wrapper">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							
		<li class="post post-ajax" <?php post_class(); ?> id="<?php echo get_page_uri($post->id); ?>">
			<a href="<?php the_permalink() ?>" data-url="<?php the_permalink() ?>" class="ajax-single">
				
				<?php if( ot_get_option('blog_thumbs') == "on" AND has_post_thumbnail() ) : ?>
					<div class="blog-thumb">
						<?php the_post_thumbnail('thumbnail'); ?>
					</div>
					<div class="blog-info">
						<div class="blog-date"><?php the_time('F jS, Y') ?></div>
						<h4><?php the_title(); ?></h4>
					</div>
					<div class="clearfix"></div>
				<?php else: ?>
					<div class="blog-date"><?php the_time('F jS, Y') ?></div>

					<h4><?php the_title(); ?></h4>

					<!-- <div class="meta"><em>by</em> <?php the_author() ?></div> -->

					<div class="blog-excerpt">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>

			</a>
		</li>
		
	<?php endwhile; ?>

	<?php else : ?>
		<h3><?php _e( 'No posts', 'kousai' ); ?></h3>
	<?php endif; ?>

	</div>	
</div>
					