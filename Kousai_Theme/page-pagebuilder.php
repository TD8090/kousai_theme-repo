<?php
/*
Template Name: Page Builder Template
*/
?>


<?php get_header(); ?>


	<div class="kou-page home">
		<div id="inside"><br />
			<div class="page-container">
				<?php the_post(); ?>
				
        		<div class="pageheader">
					<div class="headercontent">
					    <img style="display: block; padding: 0; width: 100%; text-align: center;" src="http://td1.me/wp-content/uploads/2015/02/td1-banner-dark.png" />
<!--
 						<div class="section-container">
							<h2 class="title"></h2>
							<div class="row">
								<div class="col-md-12">
								</div>
							</div>
						</div>
 -->
					</div>
				</div>
				<div class="pagecontents">
					<div class="section color-1">
						<div class="section-container">
							<?php the_content(); ?>

							<?php wp_link_pages(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>