<?php
/*
Template Name: Works Template
*/
?>

<?php get_header(); ?>
<div class="kou-page home">
    <?php the_post(); ?>
    <div id="inside">
        <div id="works" class="page">
            <div class="page-container">
                <div class="pageheader">
                    <div class="headercontent">
                        <div class="section-container">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagecontents">
                    <?php $types = kou_get_wrktypes(); ?>

                    <?php if (count($types)>0): ?>
                    <div class="section color-1" id="filters">
                        <div class="section-container">
                            <div class="row">
                                
                                <div class="col-md-3">
                                    <h3><?php _e( 'Filter by type:', 'kousai' ); ?></h3>
                                </div>
                                
                                <?php if (function_exists('ot_get_option')): ?>
                                    

                                        <div class="col-md-6" id="miu-filter">
                                            <span class="btn btn-default btn-sm active" value="all"><?php _e( 'All types', 'kousai' ); ?></span>
                                            <?php foreach ($types as $type): ?>
                                                <span class="btn btn-default btn-sm" value="<?php echo $type->slug ?>"><?php echo $type->name; ?></span>
                                            <?php endforeach; ?>
                                        </div>

                                <?php else: ?>
                                        <div class="col-md-6">
                                            <select id="cd-dropdown" name="cd-dropdown" class="cd-select">
                                                <option class="filter" value="all" selected><?php _e( 'All types', 'kousai' ); ?></option>
                                                <?php foreach ($types as $type): ?>
                                                    <option class="filter" value="<?php echo $type->slug ?>"><?php echo $type->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                <?php endif; ?>
                                
                                <div class="col-md-3" id="sort">
<!--                                    <span>Sort by year:</span>
                                    <div class="btn-group pull-right"> 

                                        <button type="button" data-sort="data-year" data-order="desc" class="sort btn btn-default btn-sm"><i class="fa fa-sort-numeric-asc"></i></button>
                                        <button type="button" data-sort="data-year" data-order="asc" class="sort btn btn-default btn-sm"><i class="fa fa-sort-numeric-desc"></i></button>
                                    </div>-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="section color-2" id="wrk-grid">
                        <div class="section-container">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wkitems">
                                        <?php
                                        //setup new WP_Query
                                        $wp_query = new WP_Query( 
                                            array(
                                                'posts_per_page'    =>    -1,
                                                'post_type'         =>    'works'
                                            )
                                        );
                                        $counter = 0;
                                        //begin loop
                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        $meta= get_post_custom($post->ID);

                                        ?>


<div class="item mix <?php kou_taxonomy_name('slug'); ?>"

<?php if (array_key_exists('kou_wrk_year', $meta)):  ?>
    data-year="<?php echo $meta['kou_wrk_year'][0] ?>"
<?php endif; ?>   >

    <div class="workmain">
        <div class="workassets">

            <?php if ($post->post_content!="") : ?>
            <!--<a href="#" class="workcollapse">
                <i class="fa fa-plus-square-o"></i>
            </a>-->
            <?php endif; ?>

            <?php if (array_key_exists('kou_wrk_ext_link', $meta)):  ?>
            <a href="<?php echo $meta['kou_wrk_ext_link'][0] ?>" class="tooltips" title="External link" target="_blank">
                <i class="fa fa-external-link"></i>
            </a>
            <?php endif; ?>

            <?php if (array_key_exists('kou_wrk_git_link', $meta)):  ?>
            <a href="<?php echo $meta['kou_wrk_git_link'][0] ?>" class="tooltips" title="Git link" target="_blank">
                <i class="fa fa-git"></i>
            </a>
            <?php endif; ?>
			
           <?php if (array_key_exists('kou_wrk_album_link', $meta)):  ?>
            <a href="<?php echo $meta['kou_wrk_album_link'][0] ?>" class="tooltips" title="Album link" target="_blank">
                <i class="fa fa-picture-o"></i>
            </a>
            <?php endif; ?>

            <?php if(array_key_exists('kou_wrk_docfile', $meta)): ?>
            <a href="<?php echo $meta['kou_wrk_docfile'][0] ?>" class="tooltips" title="<?php _e( 'Download', 'kousai' ); ?>" target="_blank">
                <i class="fa fa-cloud-download"></i>
            </a>
            <?php endif; ?>

        </div>

        <h3 class="worktitle"><?php echo $meta['kou_wrk_title'][0] ?></h3>

        <?php kou_taxonomy_name('name'); ?>

        <div class="wrkauthor"><strong>Authors:</strong> <?php echo $meta['kou_wrk_authors'][0] ?></div>


   
<!--conditional for displaying works meta info: citation-->
            <?php if (array_key_exists('kou_wrk_cit', $meta)):  ?>
        <div class="wrkcite"><strong>Cite:</strong> <?php echo $meta['kou_wrk_cit'][0] ?></div>
            <?php endif; ?>
			
            <?php if (array_key_exists('kou_wrk_year', $meta)):  ?>
        <div class="wrkcite"><strong>Updated:</strong> <?php echo $meta['kou_wrk_year'][0] ?></div>
            <?php endif; ?>
			
        <div class="assoc_tech"><strong>Tech:</strong> <?php echo $meta['kou_wrk_assoc_tech'][0] ?></div>

            <?php if ( has_post_thumbnail() ) { ?>
        <div class="wrkthumb">
            <?php the_post_thumbnail(); ?>
        </div>
            <?php } ?>

    </div>
    <?php if ($post->post_content!="") : ?>
    <div class="workdetails">
        <?php the_content(); ?>
    </div>
    <?php endif; ?>
</div>

                                        <?php endwhile; // end of the loop. ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php get_footer(); ?>


