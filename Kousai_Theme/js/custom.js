(function($, window, document, undefined){
	

	$.fn.exists = function () {
	    return this.length > 0 ? this : false;
	}


	//Handle window resize
	var resizeFlag=0;  
	$(window).resize(function(){
	    resizeFlag=1;
	})
	checkSizeChange();
	function checkSizeChange(){
	    if (resizeFlag) {
	      resizeFlag=0;
	      $(window).trigger('resized');
	    }
	    setTimeout(function(){
	      checkSizeChange();
	    }, 200);
	}



	$(window).on('newContent',function(){
		initiate();
	});

	$(document).ready(function(){
		initiate();
	});

	function initiate(){
		//tooltips
		$(".tooltips").tooltip();

		//accordion setup
		$("ul.timeline").children().eq(0)
			.find(".text").slideDown()
			.addClass("open");
			
		//labcarousel init
		generateLabCarousel();
		//remix the works
		buildMixitup();
		//magnific on gallery
		magnificPopup();
		//masonry for gallery
		galleyMasonry();
	}


	var generateLabCarousel= function() {
		
		prepareCarousel();
		function prepareCarousel(){
			var $carouselContainer = $('#labp-heads-wrap'),
				$dummyItems=$('.dummy-lab-item'),
				$imageWrapper=$('#lab-carousel'),
				$contentWrapper=$('#lab-details');

			$dummyItems.each(function(){
				$(this).children().eq(0).appendTo($imageWrapper);
				$(this).children().eq(0).appendTo($contentWrapper);
				$(this).remove();
			});
		}
		var defaultCss = {
			width: 100,
			height: 100,
			marginTop: 50,
			marginRight: 0,
			marginLeft: 0,
			opacity: 0.2
		};
		var selectedCss = {
			width: 150,
			height: 150,
			marginTop: 30,
			marginRight: -25,
			marginLeft: -25,
			opacity: 1
		};
		var aniOpts = {
			queue: false,
			duration: 300,
			//easing: 'cubic'
		};
		var $car = $('#lab-carousel');
		$car.find('img').css('zIndex', 1).css( defaultCss );

		$car.children('div').each(function(i){
			$(this).data('index',i);
		});

		$car.carouFredSel({
			circular: true,
			infinite: true,
			width: '100%',
			height: 250,
			items: {
				visible:3,
				start:1
			},
			prev: '#prev',
			next: '#next',
			auto: false,
			swipe : {
				onTouch :true,
				onMouse :true
			},
			scroll: {
				items: 1,
				duration: 300,
				//easing: 'cubic',
				onBefore: function( data ) {
					var $comming = data.items.visible.eq(1),
						$going = data.items.old.eq(1),
						$commingdetail = $("div#lab-details div").eq($comming.data('index')),
						$goingdetail = $("div#lab-details div").eq($going.data('index'));

					
					$goingdetail.fadeOut(100,function(){
						$goingdetail.siblings().hide();
						$commingdetail.fadeIn(300);
					});
					

					$comming.find('img').css('zIndex', 2).animate( selectedCss, aniOpts );
					data.items.old.eq(1).find('img').css('zIndex', 1).animate( defaultCss, aniOpts );
				}
			}

		});
	}

	var magnificPopup = function(){
		$('.popup-with-move-anim').magnificPopup({
			type: 'image',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 400,
			mainClass: 'my-mfp-slide-bottom'
		});
	}

	//depricated since 1.4.0
	// var sterllarIt = function(){
	// 	$(".stellar").stellar({
	//   		verticalOffset: -100,
	//   		horizontalScrolling: false,
	// 	});
	// }

	var galleyMasonry = function(){
		
		var $container = $('#grid');
		// initialize
		$container.imagesLoaded(function(){
			$container.masonry({
			  itemSelector: 'li'
			});
		});
		
	}

	
	/**
	*	Utility function to add extra css and js to the DOM
	*	$head : the cached jquery object of <head> of parent page
	*	$foot : the cached jquery object of footer of parent page
	*	$dt : htmlDoc of the loaded page
	**/
	var apperndCssJs = function($head,$foot,$dt){
		
		
		//handel linked styles
		$dt.find("link").each(function() {

			var cssLink = $(this).attr('href');
			var isnew = 0;

			if ($head.children('link[href="' + cssLink + '"]').length == 0 && $foot.children('link[href="' + cssLink + '"]').length == 0)
				isnew = 1;
			if (isnew == 1)
				$head.append($(this)[0]);
			
		});

		// handel linked scripts files
		$dt.find("script").each(function() {
			
			//console.log($(this)[0]);
			var jsLink = $(this).attr('src');
			
			//we dont have bussiness with inline scripts of new page
			if (typeof jsLink != 'undefined'){
				if ($head.children('script[src="' + jsLink + '"]').length == 0 && $foot.children('script[src="' + jsLink + '"]').length == 0){
					$foot.append($(this)[0]);
				}	
			}
		});
	}

	/*++++++++++++++++++++++++++++++++++++
		Works page filter functionality and class add/remove
	++++++++++++++++++++++++++++++++++++++*/


	var buildMixitup = function(){
		$('div#wrk-grid').mixitup({
			layoutMode: 'list',
			easing : 'snap',
			transitionSpeed :600,
			onMixEnd: function(){
				$(".tooltips").tooltip();
			}
		}).on('click','div.workmain',function(){
			var $this = $(this),
				$item = $this.closest(".item");
			$('div.workdetails').not($this.closest(".item").find('div.workdetails')).slideUp();
			$this.closest(".item").find('div.workdetails').slideToggle("fast","swing"/* , function(){
				$('.kou-page').perfectScrollbar('update'); */
/* 
				var $self = $(this);
			    var offset = $('div.workdetails').offset();
			    var offtop = offset.top + $self.closest(".item").height();
			    var pheight = $('#inside').height()
			    var scroll = offtop - pheight;
			    $('.kou-page').animate({scrollTop:scroll}, 500); */
/* 				$('.kou-page').animate({scrollTop: $self.parent().offset().top},750);
 */
			/* } */
			);

/* console.log($('#main').height());
console.log($('.page').height());
console.log($('.home').height());
console.log($('#inside').height());
console.log($('#works').height());
console.log($('div.workdetails').height());
console.log($('.kou-page').height());
console.log($(document).height());
console.log($(window).height());
 */
 			// $('.kou-page').scrollTo($(this), 800);
			// $.scrollTo(this);
			$('.kou-page').perfectScrollbar('update');
 		});
/* 		$('div.workmain').click(function(){
            $.scrollTo(this)                                                 
        }); */
		
		

		//default filtering based on theme options
		if ( wrksFilter != 'false'){
			$('div#wrk-grid').mixitup('filter',wrksFilter);
			$("#miu-filter").find("[value='"+wrksFilter+"']").addClass('active').siblings().removeClass('active');
		}
		

		$( '#cd-dropdown' ).dropdownit( {
			gutter : 0
		} );

		$("[name=cd-dropdown]").on("change",function(){
			var item = this.value;		
			$('div#wrk-grid').mixitup('filter',item);
		});

		$("#miu-filter").on('click','span',function(){
			
			var item = $(this).attr('value');
			console.log(item);
			$('div#wrk-grid').mixitup('filter',item);
			$(this).addClass('active').siblings().removeClass('active');
		})
	}

	

	/*++++++++++++++++++++++++++++++++++++
		sidebar
	++++++++++++++++++++++++++++++++++++++*/
	var sideS, sidebar = {
		settings : {
			$side : $(".social-icons, #main-nav"),
			$main : $("#main"),
			$trigger : $("a.mobilemenu"),
			$totaltrigger : $(".social-icons, #main-nav, #main"),
		},

		init : function(){
			sideS = this.settings;
			this.bindUiActions();

			//Remove Social icons
			var $socialIcons=$('.social-icons');
			if ($socialIcons.find('li').length==0){
				$socialIcons.css('display','none');
				$('#main-nav').css('bottom','0px');
			}
		},

		isIn : function(){
			var isIn = false;
			if (sideS.$main.hasClass("sideIn"))
				isIn = true;
			return isIn;
		},

		bindUiActions : function(){
			var self = this;
			sideS.$trigger.click(function(){
				if (self.isIn()){
					self.sideOut();
				}else{
					self.sideIn();
				}
			});
			sideS.$totaltrigger.click(function(){
				if ($(window).width() < 960 && self.isIn())
					self.sideOut(); 
			});

			$(window).on('resized',function(){
				var winWidth = $(window).width();
				//console.log(winWidth);
				if ( $(window).width() > 960 ){
					self.reset();
				}else{
					self.gomobile();
				}
			});
		},

		sideIn : function() {
			var self = this;
			var SidebarAnimIn = new TimelineLite({onComplete:function(){
				//console.log('side is in');
			}});
			SidebarAnimIn
				.to(sideS.$side,0.2,{left:0})
				.to(sideS.$main,0.2,{left:250,right:-250},"-=0.2");
			sideS.$main.addClass('sideIn');
		},

		sideOut : function(){
			var self = this;
			var SidebarAnimOut = new TimelineLite({onComplete:function(){
				//console.log('side is out');
			}});
			SidebarAnimOut
				.to(sideS.$side,0.2,{left:-250})
				.to(sideS.$main,0.2,{left:0,right:0},"-=0.2");
			sideS.$main.removeClass('sideIn');
		},

		reset : function(){
			sideS.$main.css({left:250, right:0});
			sideS.$side.css({left:0});
		},

		gomobile : function (){
			sideS.$main.css({left:0, right:0});
			sideS.$side.css({left:-250});	
		}

	}
	sidebar.init();





	/*++++++++++++++++++++++++++++++++++++++++++++++
		custom scrolls with perfectScroll plugin
	++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	perfectScrollIt();
	$(window).on('newContent',function(){
		perfectScrollIt();		
	});

	function perfectScrollIt(){

		var $main_nav=$('#main-nav'),
			$kou_page=$('.kou-page');

		$main_nav.perfectScrollbar({
			wheelPropagation:true,
			wheelSpeed:80
		});

		if (perfectScroll=='on'){
			$kou_page.perfectScrollbar({
				wheelPropagation:true,
				wheelSpeed:80
			});			
		}
	}
	



	/*++++++++++++++++++++++++++++++++++++
		click event on ul.timeline titles
	++++++++++++++++++++++++++++++++++++++*/
	$("body").on("click","ul.timeline li", function(){
		$this = $(this);
		$this.find(".text").slideDown();
		$this.find(".text").addClass("open");
		$this.siblings('li').find(".text").slideUp();
		$this.siblings('li .text').removeClass("open");
	}).on('mouseenter','ul.timeline li',function(){
		$this = $(this);
		var anim = new TweenLite($(this).find(".subject"),0.4,{'padding-left':20, paused:true});
		($this.hasClass('open')) || anim.play();
	}).on('mouseleave','ul.timeline li',function(){
		var anim = new TweenLite($(this).find(".subject"),0.2,{'padding-left':0});
	});

	/*++++++++++++++++++++++++++++++++++++
		ul-withdetails details show/hide
	++++++++++++++++++++++++++++++++++++++*/
	
	$(window).on('newContent', function(){
		$("ul.ul-withdetails .imageoverlay").css("left",'-100%');
	});
	
	$("body").on('click','ul.ul-withdetails li .row',function(){
		$(this).closest("li").find(".details")
	        .stop(true, true)
	        .animate({
	            height:"toggle",
	            opacity:"toggle"
	        },300);
	}).on('mouseenter','ul.ul-withdetails li .row',function(){
		$this = $(this);
		var anim = new TweenLite($(this).closest("li").find(".imageoverlay"),0.4,{left:0});
	}).on('mouseleave', 'ul.ul-withdetails li .row', function(){
		var anim = new TweenLite($(this).closest("li").find(".imageoverlay"),0.2,{left:"-102%"});
	});

	/*++++++++++++++++++++++++++++++++++++
		gallery overlays and popups
	++++++++++++++++++++++++++++++++++++++*/ 
	$("body").on("mouseenter",".grid li",function(){
		new TweenLite($(this).find(".over"),0.4,{bottom:0,top:0});
	}).on("mouseleave",".grid li",function(){
		new TweenLite($(this).find(".over"),0.4,{bottom:"-100%", top:"100%"});
	});




	var bs,blog={
		settings : {
			triggerAnchor : $('.ajax-single'),
			triggerDiv : $('.post-ajax'),
			singleContainer : $("#ajax-single-post"),
			blogNavigation : $("#blog-navigation")
		},
		init: function(){

			bs=this.settings;
			this.handlePaginationOnSinglePage();
			this.bindUiActions();
			this.prepareLayout();
			this.handleComments();
			this.perfectScrollIt('sidebar');
			this.perfectScrollIt();
			

		},
		decide : function(){
			
			var hash = window.location.hash;

			if ( hash == "")
			{
				bs.triggerAnchor.first().trigger('firstload');
			}else{
				this.getByHash(hash);
			}
		},
		bindUiActions: function(){
			var self = this;

			$(window).on('blogdecide',function(){
				self.decide();
			});

			
			$("body").on('click firstload','.post-ajax',function(e){
				
				
				if (blogAjaxState != "off" || e.type != 'click'){
					
					e.preventDefault();
					var url = $(this).children('a').attr('data-url');
					var hash = "#"+$(this).attr("id");
					self.makeAjax(url,hash);
					
					$(this).addClass('active').siblings().removeClass('active');
				}
					
			});	
			
			

			$('window').on('resized',function(){
				self.prepareLayout();
			});

			$("body").on('newPost',function(){
        		self.handleComments();
        		self.handlePaginationOnSinglePage();
        		self.perfectScrollIt();
			});
			


			
			//blogbar hide
			$("body").on( 'click',"a#hideshow",function(event){
			    event.preventDefault();
			    if ($(this).hasClass("isOut") ) {
			    	$(this).children("span").fadeOut();
			    	$(this).children("i").addClass('fa-chevron-circle-right').removeClass('fa-chevron-circle-left');
			    	//TweenLite.to($(this),0.5,{top:-35, ease:Power2.easeOut});
			        TweenLite.to($("#blogbar"),0.5,{top:0, ease:Power2.easeOut});
			        //TweenLite.to($("#blog-content"),0.5,{width:"75%", ease:Power2.easeOut});
			        $(this).removeClass("isOut");
			    } else {
			    	$(this).children("span").fadeIn();
			    	$(this).children("i").addClass('fa-chevron-circle-left').removeClass('fa-chevron-circle-right');
			    	//TweenLite.to($(this),0.5,{top:0, ease:Power2.easeOut});
			        TweenLite.to($("#blogbar"),0.5,{top:-$("#blogbar").height()+25, ease:Power2.easeOut});
			        //TweenLite.to($("#blog-content"),0.5,{width:"100%", ease:Power2.easeOut});
			        $(this).addClass("isOut");
			    }
			    return false;
			});
			
			//if you want to hide the blog sidebar by default, uncomment this
			// $(document).ready(function(){
			// 	$("a#hideshow").trigger('click');	
			// });


			$(window).on('resized',function(){
				self.prepareLayout();
			});
			
		},
		getByHash: function(hash){

			$div = bs.triggerDiv.filter(hash);
			if ($div.length == 1){
				$div.trigger('click');
			}else{
				var url = siteUrl+hash.substring(1);
				this.makeAjax(url,hash); 
			}
		},
		makeAjax: function(url,hash){


			var self= this;

			$.ajaxSetup({cache:false});
			self.appendLoading(bs.singleContainer.parent());
			TweenLite.to(bs.singleContainer,0.5,{top:-200,autoAlpha:0, onComplete:function(){
				bs.singleContainer.css('top',"200px");
				
				$.ajax({
					type: "GET",
					url: url,
					dataType: "html",
					success: function (response) {

						//current head and foot of the page
						var $head = $('head'),
							$foot = $("#koufooter");

					

						//add extera css and js to the DOM
						apperndCssJs($head,$foot,$(response));

						//update content
						bs.singleContainer.html(
							$(response).find("#blog-content").html()
						);
						self.removeLoading(bs.singleContainer.parent());
						TweenLite.to(bs.singleContainer,0.5,{top:0,autoAlpha:1});
						
						//update page title
						var pageTitle = $(response).find("h2.title").text();
						if (typeof pageTitle != 'undefined'){
							document.title = pageTitle;
						}

						//update url
						window.location.hash = hash;
						
						//trigger 
						$('body').trigger('newPost');    
					},
					error: function (xhr, ajaxOptions, thrownError) {
						//alert(thrownError);
						self.removeLoading(bs.singleContainer.parent());
						bs.triggerAnchor.first().trigger('click');			
					}
			    });
			}});
				
		},

		appendLoading:function($el){
			$el.append('<div class="loading"></div>');
		},
		removeLoading : function($el){
			$el.find('.loading').remove();
		},
		//adjust the height of the sidebar for archives pages
		prepareLayout : function(){
			$("#archive-content").height($(document).height() - $('#archive-header').outerHeight() - $("#blog-navigation").outerHeight());
		},
		handleComments : function(){

			var commentform=$('#commentform'); // find the comment form
		        
	        commentform.prepend('<div id="comment-status" ></div>'); // add info panel before the form to provide feedback or errors  
	        var statusdiv=$('#comment-status'); // define the infopanel
	  
	        commentform.submit(function(e){
	            
                e.preventDefault();

                //serialize and store form data in a variable
                var formdata=commentform.serialize();
                //Add a status message
                statusdiv.html('<p class="alert alert-info">Processing...</p>');
                //Extract action URL from commentform
                var formurl=commentform.attr('action');
                //Post Form with data
                $.ajax({
                        type: 'post',
                        url: formurl,
                        data: formdata,
                        error: function(XMLHttpRequest, textStatus, errorThrown){
                                statusdiv.html('<p class="alert alert-danger">You might have left one of the fields blank, or be posting too quickly</p>');
                        },
                        success: function(data, textStatus){
                                if(data=="success"){
                                    statusdiv.html('<p class="alert alert-success" >Thanks for your comment. We appreciate your response.</p>');
                                    
                                }else{
                                    statusdiv.html('<p class="alert alert-danger" >Please wait a while before posting your next comment</p>');
                                }
                                commentform.find('textarea[name=comment]').val('');
                        }
                });
                return false;
	        });
		},
		handlePaginationOnSinglePage : function(){
			
			if (typeof isSingle == 'boolean')
			{
				//see if we have pagination
				$("#blog-navigation a").each(function(){
					var url = $(this).attr('href');
					var parts = url.split( '/' );
					parts[parts.length-4] = 'blog';
					var newPathname = "";
					for ( i = 0; i < parts.length-1; i++ ) {
					  newPathname += parts[i];
					  newPathname += "/";
					}
					$(this).attr('href',newPathname);
				});
			}	
		},
		perfectScrollIt:function(){
			var scrollWrapper;
			if (arguments.length==0){
				scrollWrapper=$('#blog-content');
			}else{
				scrollWrapper=$('#archive-content');
			}
			
			scrollWrapper.scrollTop(0);
			
			if (perfectScroll=='on'){

				scrollWrapper.perfectScrollbar('destroy').perfectScrollbar({
					wheelPropagation:true,
					wheelSpeed:80
				});
			}
		}

	}

	blog.init();

	$(function() {
		$("#scroller").simplyScroll({
			auto: true,
			manualMode: 'loop',
			speed: 6
		});
	});

})(jQuery, window, document);

