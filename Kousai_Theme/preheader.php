<a id="hideshow" href="#"><i class="fa fa-chevron-circle-right"></i><span><?php _e( 'List', 'kousai' ); ?></span></a><div id="blogbar">



    <div id="postlist">
        <?php wp_reset_query(); ?>
        <?php query_posts("posts_per_page=10"); ?>


        <div class="loopclass" id="archive-content">
            <div id="scroller" class="inner-wrapper">

                <?php $counter = 0 ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


                    <li class="post post-ajax" id="<?php echo get_page_uri($post->id); ?>">
                        <a href="<?php the_permalink() ?>" data-url="<?php the_permalink() ?>" class="ajax-single">

                            <?php if( ot_get_option('blog_thumbs') == "on" AND has_post_thumbnail() ) : ?>
                                <div class="blog-thumb">
                                    <?php the_post_thumbnail('thumbnail'); ?>
                                </div>
                                <div class="blog-info">
                                    <div class="blog-date"><?php the_time('F jS, Y') ?></div>
                                    <h4><?php the_title(); ?></h4>
                                </div>
                                <div class="clearfix"></div>
                            <?php else: ?>
                                <div class="blog-date"><?php the_time('F jS, Y') ?></div>

                                <h4><?php the_title(); ?></h4>

                                <!-- <div class="meta"><em>by</em> <?php the_author() ?></div> -->

                                <div class="blog-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            <?php endif; ?>

                        </a>
                    </li>
                    <?php $counter++; ?>
                <?php endwhile; ?>

                <?php else : ?>
                    <h2><?php _e( 'No Posts', 'kousai' ); ?></h2>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
