<?php
/**
 * @package WordPress
 * @subpackage themename
 */

get_header(); ?>


<div class="kou-page home">
		<div id="inside">
			<div class="wpb_row">
				<h1 class="text-center"><?php _e( 'Looks like a 404 on the horizon...', 'kousai' ); ?></h1>
			</div>

			<div class="wpb_row">
				
				<h1 class="fac-big-title fac-title text-center"><a href="javascript:history.back()">Reverse Thrusters!</a></h1>
				
			</div>
		</div>	
	</div>
	<div id="overlay"></div>

<?php get_footer(); ?>