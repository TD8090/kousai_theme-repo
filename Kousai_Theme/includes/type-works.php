<?php 
add_action('init', 'works_register');

function works_register(){
	$works_labels = array(
	    'name' 					=> __('Works','kousai'),
	    'singular_name' 		=> __('Work','kousai'),
	    'add_new' 				=> __('Add New','kousai'),
	    'add_new_item' 			=> __("Add New Work",'kousai'),
	    'edit_item' 			=> __("Edit Work",'kousai'),
	    'new_item' 				=> __("New Work",'kousai'),
	    'view_item' 			=> __("View Work",'kousai'),
	    'search_items' 			=> __("Search Works",'kousai'),
	    'not_found' 			=> __( 'No Works found','kousai'),
	    'not_found_in_trash' 	=> __('No Works found in Trash','kousai'),
	    'parent_item_colon' 	=> ''
	);
	$works_args = array(
	    'labels' 				=> $works_labels ,
	    'public' 				=> true,
	    'publicly_queryable' 	=> true,
	    'show_ui' 				=> true, 
	    'query_var' 			=> true,
	    'rewrite' 				=> true,
	    'hierarchical' 			=> false,
	    'menu_position' 		=> null,
	    'capability_type' 		=> 'post',
	    'supports' 				=> array('title', 'editor', 'thumbnail'),
	    //'menu_icon' 			=> get_bloginfo('template_directory') . '/images/photo-album.png' //16x16 png if you want an icon
	); 
	register_post_type('works', $works_args);
	flush_rewrite_rules( false );
};

add_action( 'init', 'kou_create_works_taxonomies', 0);
 
function kou_create_works_taxonomies(){
    register_taxonomy(
        'wrktype', 'works',
        array(
            'hierarchical'=> true, 
            'label' => 'Work Types',
            'singular_label' => 'Work Type',
            'rewrite' => true
        )
    );    
}

add_action('manage_posts_custom_column', 'kou_custom_columns');
add_filter('manage_edit-works_columns', 'kou_add_new_works_columns');
function kou_add_new_works_columns( $columns ){
    $columns = array(
        'cb'		=> '<input type="checkbox">',
		'title'		=> 'Work Title',
		'wrktype'	=> 'Work Type',
		'date'		=> 'Date'
    );
    return $columns;
}

function kou_custom_columns( $column ){
    global $post;
    
    switch ($column) {
        case 'wrktype' : echo get_the_term_list( $post->ID, 'wrktype', '', ', ',''); break;
    }
}


function kou_get_wrktypes(){
	$taxonomy = 'wrktype';
	return get_terms($taxonomy);
}

function kou_taxonomy_name($type){
     global $post;
 
    $terms = get_the_terms( $post->ID , 'wrktype' );
    if ($type =="slug"){
	    foreach ( $terms as $termphoto ) { 
	        echo ' '.$termphoto->slug; 
	    }
	}else{
		foreach ( $terms as $termphoto ) { 
	        echo '<span class="label label-warning">'.$termphoto->name.'</span>'; 
	    }
	} 
}

