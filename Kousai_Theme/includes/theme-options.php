<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passed to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => 'test'
    ),
    'sections'        => array( 
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'social_icons',
        'title'       => 'Social icons'
      ),
//      array(
//        'id'          => 'fa_colors',
//        'title'       => 'Theme Colors'
//      ),
//      array(
//        'id'          => 'typography',
//        'title'       => 'Typography'
//      ),
      array(
        'id'          => 'blog',
        'title'       => 'Blog'
      ),
      array(
        'id'          => 'works',
        'title'       => 'Works'
      ),
      array(
        'id'          => 'etc',
        'title'       => 'Misc'
      )
    ),

    /* ^^^^ SECTIONS ^^^^
    /  vvvv SETTINGS vvvv */

    'settings'        => array( 
      array(
        'id'          => 'person_name',
        'label'       => 'Your Name',
        'desc'        => 'Type in your complete name here',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'sub_title',
        'label'       => 'Sub-title',
        'desc'        => 'A sub text just below your name, can be your university name, etc',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      //new option brand_logo
      array(
        'id'          => 'brand_logo',
        'label'       => 'Brand logo',
        'desc'        => 'Upload your brand logo here.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'personal_photo',
        'label'       => 'Personal photo',
        'desc'        => 'Upload your personal photo here. It will be shown at the top of the sidebar.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'cv_file',
        'label'       => 'CV file',
        'desc'        => 'Upload your CV file',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_facebook',
        'label'       => 'Facebook',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_facebook_url',
        'label'       => 'Facebook url',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_facebook:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_twitter',
        'label'       => 'Twitter',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_twitter_url',
        'label'       => 'Twitter URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_twitter:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_linkedin',
        'label'       => 'LinedIn',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_linkedin_url',
        'label'       => 'LinkedIn URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_linkedin:is(on)',
        'operator'    => 'and'
      ),
//      array(
//        'id'          => 'si_academia',
//        'label'       => 'Academia.edu',
//        'desc'        => '',
//        'std'         => 'off',
//        'type'        => 'on-off',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_academia_url',
//        'label'       => 'Academia.edu URL',
//        'desc'        => '',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => 'si_academia:is(on)',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_rg',
//        'label'       => 'Researchgate Profile',
//        'desc'        => '',
//        'std'         => 'off',
//        'type'        => 'on-off',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_rg_url',
//        'label'       => 'Researchgate URL',
//        'desc'        => '',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => 'si_rg:is(on)',
//        'operator'    => 'and'
//      ),


      array(
        'id'          => 'si_email',
        'label'       => 'Email',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_email_address',
        'label'       => 'Email address',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_email:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_gplus',
        'label'       => 'Google+',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_gplus_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_gplus:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'si_youtube',
        'label'       => 'YouTube',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_youtube_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_youtube:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'si_instagram',
        'label'       => 'Instagram',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'si_instagram_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_icons',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'si_instagram:is(on)',
        'operator'    => 'and'
      ),

//      array(
//        'id'          => 'si_flickr',
//        'label'       => 'Flickr',
//        'desc'        => '',
//        'std'         => 'off',
//        'type'        => 'on-off',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_flickr_url',
//        'label'       => 'URL',
//        'desc'        => '',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => 'si_flickr:is(on)',
//        'operator'    => 'and'
//      ),
//
//      array(
//        'id'          => 'si_pinterest',
//        'label'       => 'Pinterest',
//        'desc'        => '',
//        'std'         => 'off',
//        'type'        => 'on-off',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_pinterest_url',
//        'label'       => 'URL',
//        'desc'        => '',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => 'si_pinterest:is(on)',
//        'operator'    => 'and'
//      ),

//      array(
//        'id'          => 'si_rss',
//        'label'       => 'RSS',
//        'desc'        => '',
//        'std'         => 'off',
//        'type'        => 'on-off',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'si_rss_url',
//        'label'       => 'URL',
//        'desc'        => '',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'social_icons',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => 'si_rss:is(on)',
//        'operator'    => 'and'
//      ),

      array(
        'id'          => 'blog_ajax',
        'label'       => 'Use Ajax for blog pages?',
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_twitter',
        'label'       => 'Show Post twitter share',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_gp',
        'label'       => 'Show Post Google plus',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_fb',
        'label'       => 'Blog facebook',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_author',
        'label'       => 'Show post author',
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_tags',
        'label'       => 'Show post tags',
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_thumbs',
        'label'       => 'Show thumbs at blog list',
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'wrk_filter_preset',
        'label'       => 'Preset filtering on:',
        'desc'        => 'After adding some "works types" type the slug of the type which you want to be used as active filter, leave this blank to ignore it.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'works',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'wrk_layout',
        'label'       => 'Work items layout',
        'desc'        => '',
        'std'         => 'default',
        'type'        => 'radio',
        'section'     => 'works',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'default',
            'label'       => 'Default',
            'src'         => ''
          ),
          array(
            'value'       => 'compact',
            'label'       => 'compact',
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'wrk_filter',
        'label'       => 'Work filter layout',
        'desc'        => '',
        'std'         => 'dropdown',
        'type'        => 'radio',
        'section'     => 'works',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'dropdown',
            'label'       => 'DropDown',
            'src'         => ''
          ),
          array(
            'value'       => 'inline',
            'label'       => 'Inline badges',
            'src'         => ''
          )
        )
      ),


      array(
        'id'          => 'etc_analytics_code',
        'label'       => 'Analytics script',
        'desc'        => 'Paste your Google analytics ( or other services ) code here',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'etc',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'etc_fav_icon',
        'label'       => 'Site favicon',
        'desc'        => 'Upload a 16x16 or 32x32 .png or .ico file',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'copyright',
        'label'       => 'Copyright text',
        'desc'        => '',
        'std'         => 'Copyright text here',
        'type'        => 'text',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'no_perfect_scroll',
        'label'       => 'Use fancy scrollbars?',
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'circle_around_logo',
        'label'       => 'Circled profile picture?',
        'desc'        => 'default is on which means it will wrap your image with a circle.',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}